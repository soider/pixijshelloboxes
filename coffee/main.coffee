class DirectionManager
    constructor: ->
        @TOP = 1
        @BOTTOM = 2
        @LEFT = 4
        @RIGHT = 8
        @direction = 0
        @blockedDirection = 0
        @blocks = {}

    addDirection: (d) ->
        @direction = @direction | d

    removeDirection: (d) ->
        @direction = @direction & ~d

    handleUpTop: ->
        @removeDirection @TOP

    handleUpBottom: ->
        @removeDirection @BOTTOM

    handleUpLeft: ->
        @removeDirection @LEFT

    handleUpRight: ->
        @removeDirection @RIGHT

    handleDownTop: ->
        @addDirection @TOP

    handleDownBottom: ->
        @addDirection @BOTTOM

    handleDownLeft: ->
        @addDirection @LEFT

    handleDownRight: ->
        @addDirection @RIGHT

    blockTop: (by_sprite) ->
        @blockDirection by_sprite, @TOP
    blockBottom: (by_sprite) ->
        @blockDirection by_sprite, @BOTTOM
    blockLeft: (by_sprite) ->
        @blockDirection by_sprite, @LEFT
    blockRight: (by_sprite) ->        
        @blockDirection by_sprite, @RIGHT

    unblockTop: (by_sprite) ->
        @unblockDirection by_sprite, @TOP
    unblockBottom: (by_sprite) ->
        @unblockDirection by_sprite, @BOTTOM
    unblockLeft: (by_sprite) ->
        @unblockDirection by_sprite, @LEFT
    unblockRight: (by_sprite) ->      
        @unblockDirection by_sprite, @RIGHT


    blockDirection: (by_sprite, direction) ->
        @blockedDirection = @blockedDirection | direction
        if not @blocks[direction]
            @blocks[direction] = {count: 0}
        if not @blocks[direction][by_sprite.id]
            @blocks[direction][by_sprite.id] = true
            @blocks[direction].count++

    unblockDirection: (by_sprite, direction) ->
        if not @blocks[direction]
            @blocks[direction] = {count: 0}
        if @blocks[direction][by_sprite.id]
            @blocks[direction][by_sprite.id] = false
            if @blocks[direction].count > 0
                @blocks[direction].count--
            if @blocks[direction].count <= 0
                @blockedDirection = @blockedDirection & ~direction

class SpriteManager
    instance = null

    @getInstance: ->
        instance ?= new SpriteManager()

    constructor: ->
        @lastSpriteId = 0
        @sprites = []

    createSprite: (type, spriteName, x, y, cx, cy) ->
        texture = PIXI.Texture.fromImage spriteName
        sprite = new PIXI.Sprite texture
        sprite.id = @lastSpriteId++
        sprite.type = type
        sprite.anchor.x = cx
        sprite.anchor.y = cy
        sprite.position.x = x
        sprite.position.y = y
        @sprites.push sprite
        sprite        

    getSpritesByType: (type) ->
        sprite for sprite in @sprites when sprite.type == type

class Player

    constructor: (@spriteName, @x, @y, @cx, @cy, @stageWidth, @stageHeight) ->
        @sprite = SpriteManager.getInstance().createSprite "player", @spriteName, @x, @y, @cx, @cy
        @controls = new Controls @sprite
        document.addEventListener 'keyup', @controls.keyup
        document.addEventListener 'keydown', @controls.keydown

    CollisionVsBoxes: ()=>
        boxes = SpriteManager.getInstance().getSpritesByType "box"
        player = @sprite
        for box in boxes
            xdist = box.position.x - player.position.x
            ydist = box.position.y - player.position.y
            intersectionThresholdOrto = xThreshold = box.width / 2 + player.width/2 - 1
            intersectionThresholdDirect = xThreshold = box.width / 2 + player.width/2
            yCollision = (Math.abs xdist) <= intersectionThresholdOrto and (Math.abs ydist) <= intersectionThresholdDirect
            xCollision = (Math.abs ydist) <= intersectionThresholdOrto and (Math.abs xdist) <= intersectionThresholdDirect

            if yCollision
                if ydist < 0
                    @controls.manager.blockTop box
                else
                    @controls.manager.blockBottom box
            else
                @controls.manager.unblockTop box
                @controls.manager.unblockBottom box 
            if xCollision
                if xdist < 0
                    @controls.manager.blockLeft box
                else
                    @controls.manager.blockRight box
            else
                @controls.manager.unblockLeft box
                @controls.manager.unblockRight box

    CollisionVsWorldBorders: () =>

        if @sprite.position.x >= @stageWidth - @sprite.width/2
            @controls.manager.blockRight "world"
        else 
            @controls.manager.unblockRight "world"

        if @sprite.position.x <= 0 + @sprite.width/2
            @controls.manager.blockLeft "world"
        else
            @controls.manager.unblockLeft "world"

        if @sprite.position.y <= 0 + @sprite.height/2
             @controls.manager.blockTop "world"
        else
            @controls.manager.unblockTop "world"

        if @sprite.position.y >= @stageHeight - @sprite.height/2
            @controls.manager.blockBottom "world"
        else
            @controls.manager.unblockBottom "world"


class Controls

    constructor: (@sprite) ->
        @manager = new DirectionManager()
        @dx = 10
        @dy = 10

    keydown: (evt) =>
        if evt.keyCode == 87
            @manager.handleDownTop()
        else if evt.keyCode ==  83
            @manager.handleDownBottom()
        else if evt.keyCode == 65
            @manager.handleDownLeft()
        else if evt.keyCode == 68
            @manager.handleDownRight()

    keyup: (evt) =>
        if evt.keyCode == 87
            @manager.handleUpTop()
        else if evt.keyCode ==  83
            @manager.handleUpBottom()
        else if evt.keyCode == 65
            @manager.handleUpLeft()
        else if evt.keyCode == 68
            @manager.handleUpRight()

    moveSprite:  ->        
        sprite = @sprite
        direction = @manager.direction
        directionsBits = [@manager.TOP, @manager.BOTTOM, @manager.LEFT, @manager.RIGHT]
        directions = {}
        directions[@manager.TOP] = @moveUp
        directions[@manager.BOTTOM] = @moveDown
        directions[@manager.LEFT] = @moveLeft
        directions[@manager.RIGHT] = @moveRight
        for bit in directionsBits
            if direction & bit and not Boolean(@manager.blockedDirection & bit)
                directions[bit] sprite

    moveUp: (sprite) =>
        sprite.position.y -= @dy

    moveDown: (sprite) =>

        sprite.position.y += @dy

    moveLeft: (sprite) =>
    
        sprite.position.x -= @dx

    moveRight: (sprite) =>
        
        sprite.position.x += @dx

class Game
    @::lastSpriteId = 0

    constructor: ->
        @stage = null
        @player = null
        @controls = null
        @playerDirection = 0
        @playerBlockedDirection = 9
        @boxes = []
        @collisionManagers = []

    init: ->
        @stage = new PIXI.Stage 0xffffff
        [stageWidth, stageHeight]= [window.innerWidth, window.innerHeight]
        renderer = PIXI.autoDetectRenderer stageWidth, stageHeight
        document.body.appendChild renderer.view
        animate = =>
            requestAnimFrame animate
            if @player.controls.manager.direction
                @player.controls.moveSprite() 
                
                for collisionManager in @collisionManagers
                    collisionManager()
            renderer.render @stage
        requestAnimFrame animate
        
        @createPlayer 400, 100, stageWidth, stageHeight
        @boxes.push @addSpriteToStage SpriteManager.getInstance().createSprite "box", "enemy02.png", 200, 300, 0.5, 0.5
        @boxes.push @addSpriteToStage SpriteManager.getInstance().createSprite "box", "enemy02.png", 400, 300, 0.5, 0.5


    addSpriteToStage: (sprite) ->
        @stage.addChild sprite
        sprite

    createPlayer: (x, y, stageWidth, stageHeight) ->
        @player = new Player "player01.png", x, y, 0.5, 0.5, stageWidth, stageHeight
        @addSpriteToStage @player.sprite
        player = @player
        @collisionManagers.push @player.CollisionVsBoxes
        @collisionManagers.push @player.CollisionVsWorldBorders        

main =->
    GAME = new Game()
    window.GAME = GAME
    GAME.init()


document.addEventListener 'DOMContentLoaded', main

